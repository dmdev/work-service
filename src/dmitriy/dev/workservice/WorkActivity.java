/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workservice;

import dmitriy.dev.workservice.WorkProcessor.Result;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class WorkActivity extends FragmentActivity implements ServiceCallbackListener {
	
	WorkServiceHelper wsHelper;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		wsHelper = WorkServiceHelper.getInstance(getApplication());
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		wsHelper.addServiceCallbackListener(this);
		
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		wsHelper.removeServiceCallbackListener(this);
	}

	@Override
	public void onServiceCallback(int requestId, Result result) {

	}
	
	public void sendRequest(int requestId, WorkProcessor processor) {
		wsHelper.startRequest(requestId, processor);
	}
	
	public void cancelRequest(int requestId) {
		wsHelper.cancelRequest(requestId);
	}

	public void checkRequest(int requestId) {
		wsHelper.checkRequest(requestId);
	}
}
