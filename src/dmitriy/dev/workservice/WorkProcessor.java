/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workservice;

import java.io.Serializable;

import android.content.Context;

public abstract class WorkProcessor implements Serializable {

	public static class Result implements Serializable {
		
		public static final int NULL = 0;
		public static final int SUCCESS = 1;
		public static final int FAILURE = 2;
		public static final int PROGRESS = 3;
		
		public static final int NETWORK_ERROR = 4;
		public static final int SERVER_ERROR = 5;
		public static final int ERROR = 6;
		
		public int status = NULL;
		public String message= "";
		public int progress = 0;
		
		public static Result success() {
			Result res = new Result();
			res.status = SUCCESS;
			return res;
		}
		
		public static Result success(String message) {
			Result res = new Result();
			res.status = SUCCESS;
			res.message = message;
			return res;
		}
		
		public static Result progress(int progress) {
			Result res = new Result();
			res.status = PROGRESS;
			res.progress = progress;
			return res;
		}
		
		public static Result failure(String message) {
			Result res = new Result();
			res.status = FAILURE;
			if (message != null) res.message = message;
			return res;
		}
		
		public static Result error(String message) {
			Result res = new Result();
			res.status = ERROR;
			if (message != null) res.message = message;
			return res;
		}
		
		public static Result networkError() {
			Result res = new Result();
			res.status = NETWORK_ERROR;
			return res;
		}
		
		public static Result serverError() {
			Result res = new Result();
			res.status = SERVER_ERROR;
			return res;
		}
		
		public boolean isFinal() {
			return status != PROGRESS;
		}

	}
	
	public static interface ResultListener {
		public void onResult(Result result);
	}

	public abstract void execute(Context context, ResultListener listener);

}
