/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dmitriy.dev.workservice.WorkProcessor.Result;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class WorkService extends Service {
	
	private WorkServiceBinder binder = new WorkServiceBinder();
	private LocalBroadcastManager lm;
	private ExecutorService service = Executors.newCachedThreadPool();

	@Override
	public void onCreate() {
		super.onCreate();
		lm = LocalBroadcastManager.getInstance(this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}
	
	class WorkServiceBinder extends Binder {
		WorkService getService() {
			return WorkService.this;
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		int requestId = intent.getIntExtra(WorkServiceHelper.KEY_REQUEST_ID, -1);
		if (requestId == -1) return START_NOT_STICKY;
		
		Task task = new Task(requestId,
				(WorkProcessor) intent.getSerializableExtra(WorkServiceHelper.KEY_PROCESSOR));
		service.submit(task);
		return START_NOT_STICKY;
	}
	
	class Task implements Runnable, WorkProcessor.ResultListener {
		
		int requestId;
		WorkProcessor processor;
		
		public Task(int id, WorkProcessor processor) {
			this.requestId = id;
			this.processor = processor;
		}

		@Override
		public void run() {
			processor.execute(WorkService.this, this);
		}

		@Override
		public void onResult(Result result) {
			Intent intent = new Intent(WorkServiceHelper.ACTION);
			intent.putExtra(WorkServiceHelper.KEY_REQUEST_ID, requestId);
			intent.putExtra(WorkServiceHelper.KEY_RESULT, result);
			lm.sendBroadcast(intent);
		}
		
	}

}
