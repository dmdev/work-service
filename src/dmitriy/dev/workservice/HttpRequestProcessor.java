/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workservice;

import java.io.IOException;

import android.content.Context;

public abstract class HttpRequestProcessor extends WorkProcessor {

	@Override
	public void execute(Context context, ResultListener listener) {
		
		try {
			listener.onResult(Result.progress(0));
			handleRequest(context, listener);
		} catch (IOException e) {
			listener.onResult(Result.networkError());
		} catch (Exception e) {
			listener.onResult(Result.error(""));
			e.printStackTrace();
		} finally {
			listener.onResult(Result.progress(100));
		}
		
	}
	
	public abstract void handleRequest(Context context, ResultListener listener) throws Exception;

}
