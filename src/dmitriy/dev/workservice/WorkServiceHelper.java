/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class WorkServiceHelper {
	
	public static final String KEY_REQUEST_ID = "requestId";
	public static final String KEY_PROCESSOR = "processor";
	public static final String KEY_RESULT = "result";
	
	public static final String ACTION = "dmitriy.dev.workservice.WorkServiceHelper";
	
	private static WorkServiceHelper INSTANCE;
	private ArrayList<ServiceCallbackListener> listeners = new ArrayList<ServiceCallbackListener>();
	private Map<Integer, WorkProcessor.Result> results = new HashMap<Integer, WorkProcessor.Result>();
	private WorcServiceReceiver receiver = new WorcServiceReceiver();
	
	private Application app;

	private WorkServiceHelper(Application app) {
    	this.app = app;
		LocalBroadcastManager.getInstance(app).registerReceiver(receiver,
				new IntentFilter(ACTION));
	}
	
	public static WorkServiceHelper getInstance(Context context) {
		if (INSTANCE == null) {
			INSTANCE = new WorkServiceHelper((Application)context.getApplicationContext());
		}
		
		return INSTANCE;
	}
	
	public void cancelRequest(int requestId) {
		if (results.containsKey(requestId)) {
			results.remove(requestId);
		}
	}
	
	public void checkRequest(int requestId) {
		if (results.containsKey(requestId)) {
			dispatchResult(requestId, results.get(requestId));
		}
	}
	
	public void addServiceCallbackListener(ServiceCallbackListener listener) {
		listeners.add(listener);
	}
	
	public void removeServiceCallbackListener(ServiceCallbackListener listener) {
		listeners.remove(listener);
	}
	
	public void startRequest(int requestId, WorkProcessor processor) {
		Intent intent = new Intent(app, WorkService.class);
		intent.putExtra(KEY_REQUEST_ID, requestId);
		intent.putExtra(KEY_PROCESSOR, processor);
		app.startService(intent);
	}
	
	private void dispatchResult(int requestId, WorkProcessor.Result result) {	
		for (ServiceCallbackListener listener : listeners) {
				listener.onServiceCallback(requestId, result);
		}
	}
	
	class WorcServiceReceiver extends BroadcastReceiver {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			WorkProcessor.Result result = (WorkProcessor.Result) intent.getSerializableExtra(KEY_RESULT);
			int requestId = intent.getIntExtra(KEY_REQUEST_ID, -1);
			results.put(requestId, result);
			dispatchResult(requestId, result);
		}
	}
}
