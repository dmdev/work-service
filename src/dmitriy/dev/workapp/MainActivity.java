/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dmitriy.dev.workapp;

import dmitriy.dev.webapp.R;
import dmitriy.dev.workapp.MessageDialog;
import dmitriy.dev.workservice.WorkActivity;
import dmitriy.dev.workservice.WorkProcessor.Result;
import static dmitriy.dev.workservice.WorkProcessor.Result.*;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends WorkActivity implements OnClickListener {
	
	private static final int REQUEST_ID = 10;
	private static final String MESSAGE_DIALOG = "message_dialog";

	private TextView text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.ac_main);
		initViews();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		checkRequest(REQUEST_ID);
	}

	private void initViews() {	
		text = (TextView) findViewById(R.id.text);
		findViewById(R.id.runButton).setOnClickListener(this);
	}
	
	private void showMessage(String message) {
		MessageDialog.newInstance(message).show(
				getSupportFragmentManager(), MESSAGE_DIALOG);
	}
	
	private void showProgress() {
		setProgressBarIndeterminateVisibility(true);
	}
	
	private void hideProgress() {
		setProgressBarIndeterminateVisibility(false);
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		
		case R.id.runButton:
			sendRequest(REQUEST_ID, new LoadProcessor());
			break;

		default:
			break;
		}
		
	}
	
	@Override
	public void onServiceCallback(int requestId, Result result) {
		super.onServiceCallback(requestId, result);
		
		if (requestId == REQUEST_ID) {
			switch (result.status) {
			case PROGRESS:
				showProgress();
				text.setText(String.format("Progress: %d %%", result.progress));
				break;
				
			case SUCCESS:
				showMessage(result.message);
				break;
				
			case FAILURE:
				Toast.makeText(this, result.message, Toast.LENGTH_SHORT).show();
				break;

			default:
				break;
			}
			
			if (result.isFinal()) {
				hideProgress();
				cancelRequest(REQUEST_ID);
			}
		}
	}

}
