/*
 * Copyright 2013-2014 Dmitriy Gorbunov
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dmitriy.dev.workapp;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import dmitriy.dev.workservice.WorkProcessor;

public class LoadProcessor extends WorkProcessor {

	@Override
	public void execute(Context context, ResultListener listener) {
		
		for(int i = 0; i < 10; i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				listener.onResult(Result.progress(10*i));
			} catch (InterruptedException e) {
				e.printStackTrace();
				listener.onResult(Result.failure("failure"));
			}
		}
		
		if (new Random().nextInt()%2 == 0) {
			listener.onResult(Result.progress(100));
			listener.onResult(Result.success("Hello World!"));
		} else {
			listener.onResult(Result.failure("failure"));
		}

	}

}
